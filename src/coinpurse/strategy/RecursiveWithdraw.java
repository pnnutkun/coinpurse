package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * Recursive withdraw that use recursive pull out the money until 
 * equal the money that you want.
 * @author Pipatpol Tannavongchinda
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	private static ValueComparator comparator = new ValueComparator();
	/**
	 * Withdraw money by use Recursion.
	 * @param amount is the money that you want to pick up. 
	 * @param money is your purse.
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> money)
	{
		Collections.sort(money , comparator);
		return withdrawFrom(amount, money, 0);
	}
	/**
	 * This is a helper method of withdraw to use recursion.
	 * @param amount is the money that you want to pick up. 
	 * @param money is your purse.
	 * @param lastIndex is the size of money purse.
	 * @return List of money that you want to pick up.
	 */
	private List<Valuable> withdrawFrom(double amount , List<Valuable> money, int lastIndex)
	{
		
		if(amount < 0 )
		{
			return null;
		}
		if(lastIndex >= money.size())
		{
			return null;
		}
		if( amount - money.get(lastIndex).getValue()  == 0)
		{
			List<Valuable> out = new ArrayList<Valuable>();
			out.add(money.get(lastIndex));
			return out;
		}
		
			
		List temp = withdrawFrom(amount - money.get(lastIndex).getValue(), money, lastIndex+1);
		if( temp != null)	
		{
			temp.add(money.get(lastIndex));
		}
		else
		{
			temp = withdrawFrom(amount, money, lastIndex+1);
		}
		return temp;
	}
}
