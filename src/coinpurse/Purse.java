package coinpurse;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A Valuable purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the Valuable purse decides which
 *  money to remove.
 *  
 *  @author Pipatpol.T
 */
public class Purse extends Observable{
	/** Collection of money in the purse. */
	private List<Valuable> money;
	/** Capacity is maximum NUMBER of money the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	private WithdrawStrategy strategy;
	private static ValueComparator comparator = new ValueComparator();
	/** 
	 *  Create a purse with a specified capacity and GUI show balance and status of this purse.
	 *  @param capacity is maximum number of money you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		money = new ArrayList<Valuable>();
		BalanceGUI BGUI = new BalanceGUI();
		StatusGUI SGUI = new StatusGUI(getCapacity());
		this.addObserver(BGUI);
		this.addObserver(SGUI);
	}

	/**
	 * Count and return the number of money in the purse.
	 * This is the number of money, not their value.
	 * @return the number of money in the purse
	 */
	public int count() 
	{ 
		return money.size();	
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() 
	{
		int total_value = 0;
		for(int count = 0;count<money.size();count++)
		{
			total_value += (money.get(count)).getValue();
		}
		return total_value; 
	}


	/**
	 * Return the capacity of the Valuable purse.
	 * @return the capacity
	 */
	public int getCapacity() 
	{ 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		return money.size() == this.capacity;
	}

	/** 
	 * Insert a Valuable into the purse.
	 * The Valuable is only inserted if the purse has space for it
	 * and the Valuable has positive value.  No worthless money!
	 * @param Valuable is a Valuable object to insert into purse
	 * @return true if Valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable Valuable ) {
		// if the purse is already full then can't insert anything.
		if(Valuable.getValue()<1 || isFull())
		{
			return false;
		}
		money.add(Valuable);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of money withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		List<Valuable> withdrawList = strategy.withdraw(amount, money);
		if(withdrawList == null)
		{
			return null;
		}

		int cursor = 0;
		System.out.println("Withdrew!");
		for(Valuable out : withdrawList)
		{
			System.out.println(out.toString());
			money.remove(out);
		}

		Valuable[] temp = withdrawList.toArray(new Valuable[withdrawList.size()]);
		super.setChanged();
		super.notifyObservers(this);
		return temp;
	}
	/**
	 * Default strategy is GreedyStrategy, if Greedy is fail change it to RecursiveStrategy.
	 */
	public void setWithdrawStrategy()
	{
		strategy = new GreedyWithdraw();
		if( strategy.getClass() == GreedyWithdraw.class )
		{
			strategy = new RecursiveWithdraw();
		}

	}
	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {

		return this.count()+" money with value "+this.getBalance();
	}

}