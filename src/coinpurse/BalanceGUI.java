package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import java.awt.Font;
/**
 * The GUI shows balance in purse. 
 * @author Pipatpol Tanavongchinda
 */
public class BalanceGUI implements Observer{

	static JLabel balanceLabel;
	static JFrame purseBalanceContainer;
	
	/**
	 * Constructor that create GUI components.
	 */
	public BalanceGUI()
	{
		this.initComponents();
	}

	@Override
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse)
		{
			Purse purse = (Purse)subject;
			int balance = (int) purse.getBalance();
			balanceLabel.setText(balance + " Baht.");	
			purseBalanceContainer.pack();
			
		}
		if (info != null ) System.out.println(info);
	}
	/**
	 * Get all components of this GUI.
	 */
	 public void initComponents()
	    {
		 	purseBalanceContainer = new JFrame();
		 	purseBalanceContainer.setTitle("Purse Balance");
			JPanel purseBalancePanel = new JPanel();
	    	purseBalancePanel.setLayout(new FlowLayout());
	    	balanceLabel = new JLabel("0 Baht");
	    	balanceLabel.setFont(new Font("Tahoma", Font.PLAIN, 52));
	    	purseBalancePanel.add(balanceLabel);
	    	purseBalanceContainer.getContentPane().add(purseBalancePanel);
	    	purseBalanceContainer.setVisible(true);
	    	purseBalanceContainer.pack();
	    	purseBalanceContainer.setDefaultCloseOperation(purseBalanceContainer.EXIT_ON_CLOSE);
	    }
}
