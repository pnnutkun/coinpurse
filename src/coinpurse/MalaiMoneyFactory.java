package coinpurse;

public class MalaiMoneyFactory extends MoneyFactory {

	@Override
	public Valuable createMoney(double value) {
		if ( value < 1 ) return new Coin(value*100, "Sen");
		return new BankNote((int)value, "Ringgit");
	}

}
