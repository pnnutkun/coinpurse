package ku.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
	public static <E> void append(List<E> list, List<? extends E> args) {
		list.addAll(args);
	}
	
	public static <E extends Comparable<? super E>> E max(E ... args) {
		E max = args[0];
		for (E e : args) {
			if(e.compareTo(max) > 0){
				max = e;
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		List<Number> list = new ArrayList<Number>();
		List<Double> appendee = new ArrayList<Double>();
		list.add(1);
		appendee.add( new Double(0.5) );
		appendee.add( new Double(2.5) );
		ListUtil.append( list, appendee );
		Double max = ListUtil.max( 1.5, 2.98E8, Math.PI ,Double.MAX_VALUE); 
		System.out.println( max );
		String last = ListUtil.max( "ant", "cat", "zebra", "monkey");
		System.out.println( last );
		System.out.println( list );
	}
}
